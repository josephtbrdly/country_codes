# country_codes

A module that provides mapping between various ISO 3166-1 country code values.

## Installation
```sh
npm i country_codes
```

## Usage
### Create a new country
Strike out against The Man and make your own country!
  * All strings are trimmed
  * Alpha values are upper-cased, and limited to the last 2 or 3 provided characters
  * The numeric value will be limited to 3 digits, left-padded if needed,
    and defaults to `000` if the provided value can't be converted to an integer

```js
const ccodes = require('country_codes');

const MY_COUNTRY = new ccodes.Country({
  'name': 'Freedomville',
  'alpha_2': 'FE',
  'alpha_3': 'FRE',
  'numeric': '733'
});
JSON.stringify(MY_COUNTRY);
// {"name":"Freedomville","alpha_2":"FE","alpha_3":"FRE","numeric":"733"
```

### Get details for an existing country
Use one of the handy functions to get info about an existing country.

```js
const ccodes = require('country_codes');
// From an ISO alpha 2 code
const US = ccodes.from_alpha_2('US');
// {"name":"United States of America","alpha_2":"US","alpha_3":"USA","numeric":"840"}

// From an ISO alpha 3 code
const FRA = ccodes.from_alpha_3('FRA');
// {"name":"France","alpha_2":"FR","alpha_3":"FRA","numeric":"250"}

// From an ISO numeric code
const JEY = ccodes.from_numeric(832);
// {"name":"Jersey","alpha_2":"JE","alpha_3":"JEY","numeric":"832"}

// Maybe you don't know what kind of code the user will provide?
const WTF = ccodes.from_code('840');
// {"name":"United States of America","alpha_2":"US","alpha_3":"USA","numeric":"840"}

```

