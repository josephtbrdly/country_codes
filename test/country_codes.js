"use strict";
const assert = require('assert');
const should = require('chai').should;
const country_codes = require('../');

describe('country_codes', function () {
  "use strict";

  describe('#Country', function () {
    it('Can create an empty Country object', function () {
      let country_obj = new country_codes.Country();
      [country_obj.name, country_obj.alpha_2, country_obj.alpha_3, country_obj.numeric].map(function (prop_value) {
        assert.equal(prop_value, undefined);
      });
    });

    it('A numeric value that does not contain numbers defaults to `000`', function () {
      assert.equal(new country_codes.Country({ 'numeric': 'abc' }).numeric, '000');
      assert.equal(new country_codes.Country({ 'numeric': '' }).numeric, '000');
    });

    it('Numeric values are padded to 3 digits', function () {
      assert.equal(new country_codes.Country({ 'numeric': '1' }).numeric, '001');
      assert.equal(new country_codes.Country({ 'numeric': 1 }).numeric, '001');
      assert.equal(new country_codes.Country({ 'numeric': '13121' }).numeric, '121');
    });

    it('Country name is trimmed, but casing is not changed', function () {
      assert.equal(new country_codes.Country({ 'name': ' United States    ' }).name, 'United States');
    });

    it('Alpha-2 values are trimmed, upper-cased, and limited to 2 characters', function () {
      assert.equal(new country_codes.Country({ 'alpha_2': ' Us    ' }).alpha_2, 'US');
      assert.equal(new country_codes.Country({ 'alpha_2': ' aus    ' }).alpha_2, 'US');
    });

    it('Alpha-3 values are trimmed, upper-cased, and limited to 3 characters', function () {
      assert.equal(new country_codes.Country({ 'alpha_3': ' Usa    ' }).alpha_3, 'USA');
      assert.equal(new country_codes.Country({ 'alpha_3': ' UsA    ' }).alpha_3, 'USA');
    });
  });

  describe('#from_numeric', function () {
    it('Can create a Country from a valid numeric value', function () {
      let test_data = [
        [4, { 'name': 'Afghanistan', 'alpha_2': 'AF', 'alpha_3': 'AFG', 'numeric': '004' }],
        ['16', { 'name': 'American Samoa', 'alpha_2': 'AS', 'alpha_3': 'ASM', 'numeric': '016' }]
      ];
      test_data.map(function (test_pair) {
        let [provided, expected] = test_pair;
        let country_obj = new country_codes.from_numeric(provided);
        Object.keys(expected).forEach(function (prop_name) {
          let expected_value = expected[prop_name];
          let actual_value = country_obj[prop_name];
          assert.equal(expected_value, actual_value);
        });
      })
    });

    it('Providing an invalid code will throw a ValueError', function () {
      let test_data = [
        [country_codes.from_numeric, '000'],
      ];
      test_data.map(function (test_pair) {
        let [func, param] = test_pair;
        assert.throws(
          () => { func(param) }, { name: 'ValueError' }
        );
      });
    });
  });

  describe('#from_alpha_2', function () {
    it('Can create a Country from a valid alpha-2 value', function () {
      let test_data = [
        ['AF', { 'name': 'Afghanistan', 'alpha_2': 'AF', 'alpha_3': 'AFG', 'numeric': '004' }],
        ['AS', { 'name': 'American Samoa', 'alpha_2': 'AS', 'alpha_3': 'ASM', 'numeric': '016' }]
      ];
      test_data.map(function (test_pair) {
        let [provided, expected] = test_pair;
        let country_obj = new country_codes.from_alpha_2(provided);
        Object.keys(expected).forEach(function (prop_name) {
          let expected_value = expected[prop_name];
          let actual_value = country_obj[prop_name];
          assert.equal(expected_value, actual_value);
        });
      })
    });

    it('Providing an invalid code will throw a ValueError', function () {
      let test_data = [
        [country_codes.from_alpha_2, 'ZZ'],
      ];
      test_data.map(function (test_pair) {
        let [func, param] = test_pair;
        assert.throws(
          () => { func(param) }, { name: 'ValueError' }
        );
      });
    });
  });

  describe('#from_alpha_3', function () {
    it('Can create a Country from a valid alpha-3 value', function () {
      let test_data = [
        ['AFG', { 'name': 'Afghanistan', 'alpha_2': 'AF', 'alpha_3': 'AFG', 'numeric': '004' }],
        ['ASM', { 'name': 'American Samoa', 'alpha_2': 'AS', 'alpha_3': 'ASM', 'numeric': '016' }]
      ];
      test_data.map(function (test_pair) {
        let [provided, expected] = test_pair;
        let country_obj = new country_codes.from_alpha_3(provided);
        Object.keys(expected).forEach(function (prop_name) {
          let expected_value = expected[prop_name];
          let actual_value = country_obj[prop_name];
          assert.equal(expected_value, actual_value);
        });
      })
    });

    it('Providing an invalid code will throw a ValueError', function () {
      let test_data = [
        [country_codes.from_alpha_3, 'ZZZ'],
      ];
      test_data.map(function (test_pair) {
        let [func, param] = test_pair;
        assert.throws(
          () => { func(param) }, { name: 'ValueError' }
        );
      });
    });

  });

  describe('#from_code', function () {
    it('Can create a Country from any valid code', function () {
      let test_data = [
        ['AFG', { 'name': 'Afghanistan', 'alpha_2': 'AF', 'alpha_3': 'AFG', 'numeric': '004' }],
        ['AS', { 'name': 'American Samoa', 'alpha_2': 'AS', 'alpha_3': 'ASM', 'numeric': '016' }],
        ['40', { 'name': 'Austria', 'alpha_2': 'AT', 'alpha_3': 'AUT', 'numeric': '040' }],
        [50, { 'name': 'Bangladesh', 'alpha_2': 'BD', 'alpha_3': 'BGD', 'numeric': '050' }]
      ];
      test_data.map(function (test_pair) {
        let [provided, expected] = test_pair;
        let country_obj = new country_codes.from_code(provided);
        Object.keys(expected).forEach(function (prop_name) {
          let expected_value = expected[prop_name];
          let actual_value = country_obj[prop_name];
          assert.equal(expected_value, actual_value);
        });
      })
    });

    it('Providing an invalid code will throw a ValueError', function () {
      let test_data = [
        [country_codes.from_code, 'AA']
      ];
      test_data.map(function (test_pair) {
        let [func, param] = test_pair;
        assert.throws(
          () => { func(param) }, { name: 'ValueError' }
        );
      });
    });

  });

});
