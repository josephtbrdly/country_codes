"use strict";
import * as _ from 'lodash';
const _DATA:string[][] = require('./data.json');

interface Object { [key: string]: any; }

class ValueError extends Error {
  name = 'ValueError';
  constructor(message: string) {
    super(message);
  }
}

function iter(obj: Object) {
  const _this = obj;
  const kv_pairs = Object.keys(_this);
  let i = 0;
  let prop_count = kv_pairs.length;
  return {
    next() {
      return (i < prop_count)
        ? {done: false, value: _this[kv_pairs[i++]]}
        : {done: true};
    }
  }
}

class _ObjBase implements Iterable<any> {
  [key: string]: any;
  constructor () {
    Object.defineProperty(this, '_', {
      enumerable: false,
      configurable: true,
      writable: false,
      value: {}
    });

    Object.defineProperty(this, '_setProp', {
      enumerable: false,
      configurable: false,
      value: function(property_name: string, value: any) {
        this._[property_name] = value;
      }
    });

    Object.defineProperty(this, '_getProp', {
      enumerable: false,
      configurable: false,
      value: function(property_name: string) {
        return this._[property_name];
      }
    });
  }
}

interface COUNTRY_CODE_VALUES { [key: string]: Country }

interface CountryMapping { [key: string]: string }

let ALPHA_3: COUNTRY_CODE_VALUES = {};
let ALPHA_2: CountryMapping = {};
let NUMERIC: CountryMapping = {};

/**
 * Returns a 3-digits numeric string from the provided value.
 * If the provided value is not 3 digits, it will be padded. If
 * it is longer, only the last 3 are returned. Non-numeric values
 * will be returned as 000.
 *
 * @param {*} numeric_code
 * @returns {string}
 */
function format_numeric(numeric_code: any): string {
  let c_val = '000';
  try {
    let parsed_int = parseInt(numeric_code);
    if (isNaN(parsed_int) === false) c_val += String(parsed_int);
  } catch (_er) {}
  return c_val.slice(-3);
}

export interface CountryDetail {
  [key: string]: string|undefined
  name?: string,
  alpha_3?: string,
  alpha_2?: string,
  numeric?: string
};

/**
 * A class representing a Country
 *
 * @class Country
 */
export class Country extends _ObjBase {
  /**
   * Creates an instance of Country from the provided values.
   * The provided values are not checked for validity!
   *
   * @param {CountryDetail} country_values
   * @memberof Country
   */
  constructor(country_values?: CountryDetail) {
    super();

    Object.defineProperties(this, {
      name: {
        /**
         * The name of the country
         *
         * @type {(string|undefined)}
         * @memberof Country
         */
        enumerable: true,
        configurable: false,
        get (): string|undefined {
          return this._getProp('name');
        },
        set (new_value: string|undefined) {
          this._setProp('name', _.toString(new_value).trim());
        }
      },
      alpha_2: {
        /**
         * The ISO_3166-1 Alpha-2 code for this country
         *
         * @type {(string|undefined)}
         * @memberof Country
         */
        enumerable: true,
        configurable: false,
        get (): string|undefined {
          return this._getProp('alpha_2');
        },
        set (new_value: string|undefined) {
          this._setProp('alpha_2', _.toString(new_value).trim().toUpperCase().slice(-2));
        }
      },
      alpha_3: {
        /**
         * The ISO_3166-1 Alpha-3 code for this country
         *
         * @type {(string|undefined)}
         * @memberof Country
         */
        enumerable: true,
        configurable: false,
        get (): string|undefined {
          return this._getProp('alpha_3');
        },
        set (new_value: string|undefined) {
          this._setProp('alpha_3', _.toString(new_value).trim().toUpperCase().slice(-3));
        }
      },
      numeric: {
        /**
         * The ISO_3166-1 Numeric code for this country, left-padded to 3 digits
         *
         * @type {(string|undefined)}
         * @memberof Country
         */
        enumerable: true,
        configurable: false,
        get (): string|undefined {
          return this._getProp('numeric');
        },
        set (new_value: string|undefined) {
          this._setProp('numeric', format_numeric(new_value));
        }
      }
    });

    if (typeof country_values === 'object') {
      this.name = country_values.name;
      this.alpha_2 = country_values.alpha_2;
      this.alpha_3 = country_values.alpha_3;
      this.numeric = country_values.numeric;
    }
  }

  public [Symbol.iterator]() {
    const _this = this._;
    const kv_pairs = Object.keys(_this);
    let i = 0;
    let prop_count = kv_pairs.length;
    return {
      next() {
        return (i < prop_count)
          ? {done: false, value: _this[kv_pairs[i++]]}
          : {done: true};
      }
    }
  }
}


/**
 * Creates an instance of Country from the provided ISO_3166-1 Alpha-3 code.
 * If no matching country info is found, a ValueError is thrown.
 *
 * @param {string} alpha_3
 * @throws {ValueError}
 * @returns {Country}
 */
export function from_alpha_3(alpha_3: string): Country {
  let c_val:string = _.toString(alpha_3).trim().toUpperCase();
  let country_detail = ALPHA_3[c_val];
  if (typeof country_detail !== 'undefined') {
    return new Country(country_detail);
  } else {
    throw new ValueError('The provided country code (`' + alpha_3 + '` does not match a valid country code!');
  }
}

/**
 * Creates an instance of Country from the provided ISO_3166-1 Alpha-2 code.
 * If no matching country info is found, a ValueError is thrown.
 *
 * @param {string} alpha_2
 * @throws {ValueError}
 * @returns {Country} Country
 */
export function from_alpha_2(alpha_2: string): Country {
  return from_alpha_3(ALPHA_2[_.toString(alpha_2).trim().toUpperCase()]);
}

/**
 * Creates an instance of Country the provided ISO_3166-1 numeric code.
 * If no matching country info is found, a ValueError is thrown.
 *
 * @param {(string|number)} numeric
 * @throws {ValueError}
 * @returns {Country}
 */
export function from_numeric(numeric: string|number): Country {
  return from_alpha_3(NUMERIC[format_numeric(numeric)]);
}

/**
 * Creates an instance of Country from the provided code.
 * If you know the type of code, using the other specialized
 * methods to create a country will be faster.
 * If no matching country can be found, a ValueError is thrown.
 *
 * @param {(string|number)} country_code
 * @throws {ValueError}
 * @returns {Country}
 */
export function from_code(country_code: string|number): Country {
  let country_detail: Country|undefined;
  try {
    country_detail = from_numeric(country_code);
  } catch (er) {
    try {
      country_detail = from_alpha_2(_.toString(country_code));
    } catch (_er) {
      try {
        country_detail = from_alpha_3(_.toString(country_code));
      } catch (_er) {}
    }
  }
  if (typeof country_detail !== 'undefined') return country_detail;
  throw new ValueError('No country could be found with value `' + _.toString(country_code).trim() + '`!');
}

_DATA.forEach(function(country_array) {
  let [name, alpha_2, alpha_3, numeric] = country_array;
  ALPHA_3[alpha_3] = new Country({name: name, alpha_2: alpha_2, alpha_3: alpha_3, numeric: numeric});
  ALPHA_2[alpha_2] = alpha_3;
  NUMERIC[numeric] = alpha_3;
});

